# KPET format compatibility

UPT consumes `beaker.xml`, which is created by kpet project. This file explains what
that xml must contain, so any changes to kpet don't break UPT. 

To ensure xml is UPT compatible, do following:
* check that submitting given xml to Beaker using `bkr submit` produces no fatal errors and successfully provisions hosts
* ensure that every `<task>` element has valid `<fetch>` element, so restraint knows from where to download tests
* ensure that every test has `CKI_UNIVERSAL_ID`, or `CKI_NAME` or both set; otherwise it's considered to be a setup task, not a test and its result doesn't affect the kernel testing result, unless this task aborts and the kernel to test is installed (boot task ran)
* ensure that `CKI_WAIVED` parameter with value `true` is used to denote a waived test; if the parameter is missing or set to value 'false', that test is not waived
* ensure that there is a task that has `CKI_UNIVERSAL_ID` value equal to `boot`,
  or that the name of this task is `boot test`

To explain the last point: in many cases there's a distro kernel that we don't want to test. This
kernel itself may have bugs, and we don't want to report any test results from it. Thus, a presence
of a `boot test` task signifies to UPT that a desired kernel was installed and all tasks after it
produce test results that govern overall kernel testing result. The results of all tasks prior to it are ignored.

So, if there is no need to install a new kernel before running the tests, please provide this task
regardless.
