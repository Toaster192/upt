"""Test cases for restraint_shell module."""
import copy
import os
import unittest

from bs4 import BeautifulSoup as BS

from plumbing.format import ProvisionData
from plumbing.objects import Host
from restraint_wrap.restraint_shell import ShellWrap
from tests.const import ASSETS_DIR
from upt import const


class TestShellWrap(unittest.TestCase):
    """Test cases for restraint_shell module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.prov_data = ProvisionData.deserialize_file(self.req_asset)

    def test_wrap(self):
        """Ensure ShellWrap creates a directory."""
        resource_group = self.prov_data.get_provisioner('beaker').rgs[0]
        wrap = ShellWrap(resource_group, **{'keycheck': 'no'})
        temp_dir = wrap.tempdir
        self.assertTrue(os.path.isdir(temp_dir.name))

        del wrap

        self.assertFalse(os.path.isdir(temp_dir.name))

    def test_rerun_recipe_tasks_from_index(self):
        # pylint: disable=protected-access
        """Ensure that logic to build a shorter list of tasks (for rerun) works."""
        resource_group = copy.deepcopy(self.prov_data.get_provisioner('beaker').rgs[0])
        host = resource_group.recipeset.hosts[0]

        # Keep tasks from index 2.
        host._rerun_recipe_tasks_from_index = 2
        wrap = ShellWrap(resource_group, **{'keycheck': 'no'})
        expected = '''<job><recipeSet> <recipe id="123" ks_meta="harness='restraint-rhts beakerlib
        '">  <task name="/distribution/check-install" role="STANDALONE"> <params><param name="
        CKI_NAME" value="/distribution/check-install"/></params> </task> </recipe> <recipe id=
        "456" ks_meta="harness='restraint-rhts beakerlib'"> <task name="TestUsedToCheckTestPla
        nLogic1" role="STANDALONE"> <params><param name="CKI_NAME" value="TestUsedToCheckTestPl
        anLogic1"/></params> </task> </recipe> <recipe id="789" ks_meta="harness='restraint-rhts
        beakerlib'"> <task name="TestUsedToCheckTestPlanLogic2" role="STANDALONE"> <params><para
        m name="CKI_NAME" value="TestUsedToCheckTestPlanLogic2"/></params> </task> </recipe>
        </recipeSet></job>'''.replace('\n', '').replace(' ', '')

        self.assertEqual(expected, str(wrap.run_soup).replace('\n', '').replace(' ', ''))

    def test_update_duration(self):
        """Ensure update_duration computes sets 120% of duration of tasks."""
        host = Host({'recipe_id': 1234})

        soup = BS('''<recipe>
            <task name="a1" status="Aborted">
                <param name="KILLTIMEOVERRIDE" value="10" />
            </task>
            <task name="/distribution/command" status="Cancelled">
                <param name="KILLTIMEOVERRIDE" value="20" />
            </task>
        </recipe>''', 'xml')
        ShellWrap.update_duration(host, soup.findAll('task'))

        # 120% of 30 is 36.
        self.assertEqual(host.duration, 36)

    def test_update_duration_default(self):
        """Ensure update_duration uses a hardcoded default if tasks don't have duration set."""
        host = Host({'recipe_id': 1234, 'duration': 0})
        ShellWrap.update_duration(host, [])
        # Default is set.
        self.assertEqual(host.duration, const.DEFAULT_RESERVESYS_DURATION)
