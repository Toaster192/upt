"""Test cases for aws module."""
import itertools
import json
import os
import unittest
from unittest import mock

from botocore.exceptions import ClientError

from plumbing.format import ProvisionData
from tests.const import ASSETS_DIR
from tests.const import AWS_UPT_CONFIG
from upt.const import EC2_INSTANCE_RUNNING

# pylint: disable=protected-access


class TestAWS(unittest.TestCase):
    """Testcase for AWS provisioner."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'aws_req.yaml')
        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.aws = self.provision_data.get_provisioner('aws')

    def test_update_provisioning_request(self):
        """Ensure update_provisioning_request works and sets hostname."""
        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], '_instance') as mock_inst:
            mock_inst.public_dns_name = 'hostname1'

            self.aws.update_provisioning_request(self.aws.rgs[0])
            self.assertEqual(str(list(self.aws.hosts())[0].hostname), 'hostname1')

    def test_release_resources(self):
        """Ensure release_resources works with instance."""
        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], '_instance') as mock_inst:
            self.aws.release_resources()
            mock_inst.terminate.assert_called()

    def test_release_resources_no_instance(self):
        """Ensure release_resources works with instance."""
        with mock.patch.object(self.aws.ec2, 'Instance') as mock_inst:
            mock_inst.return_value.terminate.return_value = None

            self.aws.release_resources()
            mock_inst.return_value.terminate.assert_called()

    @mock.patch.dict(os.environ, {'AWS_UPT_CONFIG': AWS_UPT_CONFIG})
    def test_aws_config(self):
        """Ensure aws_config is taken from AWS_UPT_CONFIG env. variable and is json-decoded."""
        self.assertEqual(self.aws.aws_config, json.loads(AWS_UPT_CONFIG))

    @mock.patch('upt.logger.LOGGER.debug')
    def test_provision_disabled(self, mock_debug):
        """Ensure provision works."""
        aws = ProvisionData.deserialize_file(self.req_asset).get_provisioner('aws')
        aws.rgs[0].recipeset.hosts[0].misc = {'instance_id': None}
        with mock.patch.object(aws.ec2, 'Instance') as mock_inst:
            with mock.patch.object(aws, 'provision_host', lambda *args: [mock_inst]):
                aws.provision(**{'preprovision': None})
                mock_debug.assert_called_with('* No pre-provisioning enabled.')

    @mock.patch('upt.logger.LOGGER.debug')
    def test_provision_enabled(self, mock_debug):
        """Ensure provision works."""
        with mock.patch.object(self.aws, 'provision_host', lambda *args: None):
            self.aws.provision(**{'preprovision': '0'})
            mock_debug.assert_not_called()

    def test_preprovision_invalid(self):
        """Ensure preprovisioning raises error if invalid args are specified."""
        with mock.patch.object(self.aws, 'provision_host', lambda *args: None):
            with self.assertRaises(ValueError):
                self.aws.provision(**{'preprovision': 'x,y'})

    def test_is_provisioned(self):
        """Ensure is_provisioned calls get_provisioning_state that returns 2 item tuple."""
        with mock.patch.object(self.aws, 'get_provisioning_state', lambda *args: (True, None)):
            self.assertTrue(self.aws.is_provisioned([None]))

    @mock.patch.dict(os.environ, {'AWS_UPT_CONFIG': AWS_UPT_CONFIG})
    def test_provision_host(self):
        """Ensure provision_host works / is called with correct params."""
        with mock.patch.object(self.aws.ec2, 'create_instances') as mock_create_instances:
            host = list(self.aws.hosts())[0]
            self.aws.provision_host(host)
            params = {'LaunchTemplate': {'LaunchTemplateName': self.aws.aws_config['launch_template']},
                      'TagSpecifications': [
                          {
                              'ResourceType': 'instance',
                              'Tags': [{
                                  'Key': 'Name',
                                  'Value': f'{self.aws.aws_config["instance_prefix"]}.{host.recipe_id}'
                              }]
                          }
            ],
                'MaxCount': 1,
                'MinCount': 1,
            }
            mock_create_instances.assert_called_with(**params)

    def test_reprovision_aborted(self):
        """Ensure reprovision_aborted works."""
        host2reprovision = list(self.aws.hosts())[0]
        with mock.patch.object(host2reprovision, '_instance') as mock_inst:
            with mock.patch.object(self.aws, 'provision_host') as mock_provision_host:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING + 1}
                self.aws.reprovision_aborted()

                mock_provision_host.assert_called()

    def test_no_reprovision_aborted(self):
        """Ensure reprovision_aborted isn't called when all hosts are in OK state."""
        host2reprovision = list(self.aws.hosts())[0]
        with mock.patch.object(host2reprovision, '_instance') as mock_inst:
            with mock.patch.object(self.aws, 'provision_host') as mock_provision_host:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
                self.aws.reprovision_aborted()

                mock_provision_host.assert_not_called()

    @mock.patch('cki_lib.misc.safe_popen')
    def test_get_provisioning_state(self, mock_safe_popen):
        """Ensure get_provisioning_state returns provisioned when instances are running."""
        rg2test = self.aws.rgs[0]
        mock_safe_popen.return_value = ('', '', 0)

        with mock.patch.object(self.aws.ec2, 'Instance') as mock_inst:
            mock_inst.return_value.state = {'Code': EC2_INSTANCE_RUNNING}

            provisioned, erred = self.aws.get_provisioning_state(rg2test)
            self.assertEqual(erred, [])
            self.assertTrue(provisioned)

    @mock.patch('cki_lib.misc.safe_popen')
    def test_heartbeat(self, mock_safe_popen):
        """Ensure heartbeat works."""
        rg2test = self.aws.rgs[0]
        mock_safe_popen.return_value = ('', '', 0)

        with mock.patch.object(self.aws.ec2, 'Instance') as mock_inst:
            mock_inst.return_value.state = {'Code': EC2_INSTANCE_RUNNING + 1}

            recipe_ids_dead = set()
            self.aws.heartbeat(rg2test, recipe_ids_dead)

            self.assertEqual(recipe_ids_dead, {1})

    @mock.patch('upt.logger.LOGGER.debug')
    def test_get_provisioning_state_no_instance(self, mock_debug):
        """Ensure get_provisioning_state returns not provisioned when we keep getting ClientError."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, '_instance') as mock_inst:
            mock_inst.id = 1
            client_error = ClientError(error_response={'Error': {'Code': 'InvalidInstanceID.NotFound'}},
                                       operation_name='DescribeInstances')
            mock_inst.reload.side_effect = itertools.chain([client_error], itertools.cycle([None]))

            provisioned, erred = self.aws.get_provisioning_state(self.aws.rgs[0])
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)
            mock_debug.assert_called_with('Waiting for host %s to be created', mock_inst.id)

    def test_get_provisioning_state_no_dns(self):
        """Ensure get_provisioning_state returns not provisioned when there's no public dns name for instance."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, '_instance') as mock_inst:
            mock_inst.public_dns_name = ''

            provisioned, erred = self.aws.get_provisioning_state(self.aws.rgs[0])
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)
            mock_inst.reload.assert_called()

    @mock.patch('cki_lib.misc.safe_popen')
    def test_get_provisioning_state_no_user_data(self, mock_safe_popen):
        """Ensure get_provisioning_state doesn't call ssh readiness check when user script is done."""

        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], '_user_data_done', True):
            with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], '_instance') as mock_inst:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
                self.aws.get_provisioning_state(self.aws.rgs[0])
                mock_safe_popen.assert_not_called()

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('builtins.print')
    def test_get_provisioning_state_ssh(self, mock_print, mock_debug, mock_safe_popen):
        """Ensure get_provisioning_state uses ssh readiness check when user script isn't done."""
        rg2test = self.aws.rgs[0]
        mock_safe_popen.return_value = ('some output', 'some error', 1)

        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], '_instance') as mock_inst:
            mock_inst.id = 1
            mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}

            provisioned, erred = self.aws.get_provisioning_state(rg2test)
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)

            mock_debug.assert_any_call('some error')
            mock_debug.assert_any_call('Waiting for host %s to finish UserData script', mock_inst.id)

            mock_print.assert_called_with('some output')

    def test_get_resource_ids(self):
        """Ensure test_get_resource_ids returns instance ids."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, 'misc', {'instance_id': 'i-1234'}):
            self.assertEqual(['i-1234'], self.aws.get_resource_ids())
